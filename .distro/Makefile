include Makefile.common

REDHAT:=$(shell pwd)
RPMBUILD:=$(REDHAT)/rpmbuild

# Hide command calls without debug option
ifeq ($(DEBUG),1)
  DS=
else
  DS=@
endif

KOJI_OPTIONS:=$(KOJI_FLAGS) --scratch

# Hide progress bar in scripts
ifeq ($(NOPROGRESS),1)
  KOJI_OPTIONS:=$(KOJI_OPTIONS) --noprogress
endif

# Do not wait for build finish
ifeq ($(NOWAIT),1)
  KOJI_OPTIONS:=$(KOJI_OPTIONS) --nowait
endif

# create an empty localversion file if you don't want a local buildid
ifneq ($(NOLOCALVERSION),1)
  ifeq ($(LOCALVERSION),)
    LOCALVERSION=$(shell cat ../localversion 2>/dev/null)
  endif
  ifeq ($(LOCALVERSION),)
    LOCALVERSION:=.$(shell id -u -n)$(shell date +"%Y%m%d%H%M")
  else
    LOCALVERSION:=.$(LOCALVERSION)
  endif
else
  LOCALVERSION:=
endif

.PHONY: rh-lean-sources rh-prep rh-srpm rh-rhel-koji rh-centos-koji rh-help
all: rh-help

rh-clean-sources:
	$(DS)for i in $(RPMBUILD)/SOURCES/*; do \
		rm -f $$i; \
	done;

rh-prep: rh-clean-sources
	$(DS)if [ -n "$(SOURCES_FILELIST)" ]; then \
		echo "Copying Sources: $(SOURCES_FILELIST)"; \
		cp $(SOURCES_FILELIST) $(RPMBUILD)/SOURCES; \
	fi
	$(DS)$(REDHAT)/process_patches.sh "$(TARFILE)" "$(SPECFILE)" "$(MARKER)" "$(STARTNUM)" "$(LOCALVERSION)"

rh-srpm: rh-prep
	$(DS)rpmbuild --define "_sourcedir $(RPMBUILD)/SOURCES" --define "_builddir $(RPMBUILD)/BUILD" --define "_srcrpmdir $(RPMBUILD)/SRPMS" --define "_rpmdir $(RPMBUILD)/RPMS" --define "_specdir $(RPMBUILD)/SPECS" --define "dist $(DIST)" --nodeps -bs $(RPMBUILD)/SPECS/$(SPECFILE)

rh-rhel-koji: rh-srpm
	@echo "Build $(SRPM_NAME)$(LOCALVERSION).src.rpm as $(RHEL_TARGET)"
	$(DS)brew build $(KOJI_OPTIONS) $(RHEL_TARGET) $(RPMBUILD)/SRPMS/$(SRPM_NAME)$(LOCALVERSION).src.rpm

rh-centos-koji: rh-srpm
	@echo "Build $(SRPM_NAME)$(LOCALVERSION).src.rpm as $(C9S_TARGET)"
	$(DS)koji --profile=stream build $(KOJI_OPTIONS) $(C9S_TARGET) $(RPMBUILD)/SRPMS/$(SRPM_NAME)$(LOCALVERSION).src.rpm

rh-help:
	@echo "rh-srpm:        Create srpm"
	@echo "rh-rhel-koji:   Build package using RHEL 9 koji"
	@echo "rh-centos-koji: Build package using CentOS 9 Streams koji"
	@echo "rh-help:        Print out help"
